Git Commands
============

## Quick Links
___
### GitHub Link
```https://github.com/joshnh/Git-Commands```

--

### File Path
```/mnt/c/Users/Abhisek/Projects/Learning Materials/Git```

## Commands
___
### Getting & Creating Projects

| Command | Description |
| ------- | ----------- |
| `git init` | Initialize a local Git repository |
| `git clone ssh://git@github.com/[username]/[repository-name].git` | Create a local copy of a remote repository |

--

### Check your local repository status

| Command | Description |
| ------- | ----------- |
| `git status` | Lists all the files if added or modified but not commited  |

--

### Add untracked files in staging

| Command | Description |
| ------- | ----------- |
| `git add .` | Adds all untracked files at once  |
| `git add file1.txt file2.txt` | Add one or multiple individual files |

--

### Remove added files from staging

| Command | Description |
| ------- | ----------- |
| `git rm --cached file2.txt file3.txt` | Removes added files individually |
| `git rm --cached -r .` | Removes all the added files from staging |
| `git rm --cached -f file1.txt file2.txt` | Removes the modified files from staging |

--

### Remove staged files after modification

| Command | Description |
| ------- | ----------- |
| `git restore --staged file1.txt file2.txt` | Untrack the modified files individually |

--

### Commit your changes

| Command | Description |
| ------- | ----------- |
| `git commit -m "message"` | -m stands for message |

--

### List your commits

| Command | Description |
| ------- | ----------- |
| `git log` | List all commits till current branch |
| `git log --oneline` | List all commits till current branch |

--

### Checkout to your previous commits

| Command | Description |
| ------- | ----------- |
| `git checkout commitId` | move into any specific commit |
| `git checkout master` | move back to master |

--

### Revert a commit

| Command | Description |
| ------- | ----------- |
| `git revert commitId` | move back and forth any specific commit |

--

### Reset your commits

| Command | Description |
| ------- | ----------- |
| `git reset --soft commitId` | removes the commits permanently but not the local code changes |
| `git reset --mixed commitId` | removes the commits permanently also unstage your last changes |
| `git reset --hard commitId` | removes the commits permanently along with the local code changes |

--

### Operate Branches

| Command | Description |
| ------- | ----------- |
| `git branch <branch name>` | Creates a new local branch |
| `git checkout -b <branch name>` | Create & moves inside the local branch |
| `git branch -d <branch name>` | Deletes a local branch |
| `git branch -D <branch name>` | Forcefully deletes a local branch |
| `git branch -a` | List all the local branches |

--

### Merge Branches

| Command | Description |
| ------- | ----------- |
| `git checkout master && git marge <branch name>` | Merge a particular branch from master |

--
