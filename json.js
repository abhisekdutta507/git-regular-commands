// const Collection = [
// 	{
// 		_id: "djfkjfflKskkmd",
// 		name: "Ananya Mitra",
// 		__v: "jfhLKhfesda"
// 	},
// 	{
// 		_id: "djfkjfflKskkmd",
// 		name: "Ananya Mitra",
// 		__v: "jfhLKhfesda"
// 	},
// 	{
// 		_id: "djfkjfflKskkmd",
// 		name: "Ananya Mitra",
// 		__v: "jfhLKhfesda"
// 	},
// 	{
// 		_id: "djfkjfflKskkmd",
// 		name: "Ananya Mitra",
// 		__v: "jfhLKhfesda"
// 	},
// 	{
// 		_id: "djfkjfflKskkmd",
// 		name: "Ananya Mitra",
// 		__v: "jfhLKhfesda"
// 	}
// ]

// const listWrapper = document.createElement("div");
// listWrapper.className = "list-wrapper";
// list.forEach((item) => {
//   const listElement = document.createElement("div");
//   listElement.className = "list-element";
//   listElement.textContent = item;
//   listWrapper.appendChild(listElement);
// });

// app.appendChild(listWrapper);

// 1. What is Hoisting? (NO)
// 2. What is callback function? (NO)
// 3. What is the typeof operator? (NO)
// 4. What is window? What is document? (YES)
// 5. What is DOM? (YES)
// 6. What is NaN? (NO)
// 7. What is localStorage? (YES)
// 8. What is cookie strage? (NO)

// 9. What is Service Worker? (NO)
// 10. What is Promise? (YES)
// 11. What is Promise.all()? (YES)
// 12. What is setTimeout()? (YES)
// 13. What is clearTimeout()? (YES)


function sum (a, b) {
	return new Promise((resolve) => {
		setTimeout(() => {
			resolve(a + b);
		}, 1000);
	});
}

// sum(2, 7).then((r) => console.log(r));

const list = [
	{a: 1, b: 3},
	{a: 9, b: 2},
	{a: 6, b: -4}
];

const promiseList = list.map((obj) => sum(obj.a, obj.b));

// asynchronous
Promise.all(promiseList).then((r) => console.log(r));

// Sort the students as per their priority
const priorityQueue = [
	{ id: 6, priority: 1 },
	{ id: 3, priority: 2 },
	{ id: 1, priority: 3 },
	{ id: 4, priority: 4 }
];
const students = [
	{ id: 3, name: 'Abc Def' },
	{ id: 1, name: 'Mno Pqr' },
	{ id: 4, name: 'Uvw Xyz' },
	{ id: 6, name: 'Rst Jlk' }
];

const answer = [
	{ id: 6, name: 'Rst Jlk' },
	{ id: 3, name: 'Abc Def' },
	{ id: 1, name: 'Mno Pqr' },
	{ id: 4, name: 'Uvw Xyz' }
]

const logicalAnswer = priorityQueue.map((item) => {
	return students.find((student) => student.id === item.id);
});

// synchronous
console.log('sorted students are', logicalAnswer);